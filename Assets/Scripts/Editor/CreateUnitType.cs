﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateUnitType
{
    [MenuItem("Assets/Create/UnitType")]
    static void DoIt()
    {
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        var asset = ScriptableObject.CreateInstance<UnitType>();
        AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(path + "/Unit Type.asset"));
    }
}