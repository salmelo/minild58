﻿using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour
{
    public bool worldSpace = true;

    new private Camera camera;

    // Use this for initialization
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (worldSpace)
        {
            Vector3 pos = camera.ScreenToWorldPoint(Input.mousePosition);
            pos.z = transform.position.z;
            transform.position = pos;
        }
        else
        {
            Vector3 pos = Input.mousePosition;
            pos.z = transform.position.z;
            transform.position = pos;
        }
    }
}
