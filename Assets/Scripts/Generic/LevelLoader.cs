﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    public string levelName = "MainScene";
    public bool resetTime = false;
    public bool pauseTime = false;
    public bool startTime = false;

    public string[] resetPools = new string[0];
    public bool resetOtherPools = false;

    public void LoadLevel()
    {

        if (resetTime)
        {
            TimeManager.Instance.ResetOnLoad = true;
        }

        if (pauseTime)
        {
            TimeManager.Instance.Pause();
        }
        else if (startTime)
        {
            TimeManager.Instance.Unpause();
        }

        if (resetOtherPools)
        {
            foreach (string pool in PoolManager.Instance.PoolList)
            {
                if (System.Array.IndexOf(resetPools, pool) < 0)
                {
                    PoolManager.Instance.ResetOnLoad.Add(pool);
                }
            }
        }
        else
        {
            foreach (string pool in resetPools)
            {
                PoolManager.Instance.ResetOnLoad.Add(pool);
            }
        }

        Application.LoadLevel(levelName);
    }
}
