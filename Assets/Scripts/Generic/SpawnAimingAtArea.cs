﻿using UnityEngine;
using System.Collections;

public class SpawnAimingAtArea : MonoBehaviour
{

    public GameObject spawn;
    public Transform spawnCornerA;
    public Transform spawnCornerB;
    public Transform targetCornerA;
    public Transform targetCornerB;

    public float minDelay = .5f;
    public float maxDelay = 3f;

    public LayerMask targetCheckMask = -1;
    public float targetCheckRadius = .25f;

    private RaycastHit2D[] hit = new RaycastHit2D[1];

    // Use this for initialization
    void Start()
    {
        Invoke("Spawn", Random.Range(minDelay, maxDelay));
    }

    void Spawn()
    {
        Vector3 spawnPoint;
        Vector3 targetPoint;
        bool goodTarget = false;
        do
        {
            spawnPoint = PointInBox(spawnCornerA.position, spawnCornerB.position);
            targetPoint = PointInBox(targetCornerA.position, targetCornerB.position);

            if (targetCheckMask != 0)
            {
                goodTarget = Physics2D.CircleCastNonAlloc(spawnPoint, targetCheckRadius, targetPoint
                                                        , hit, 100, targetCheckMask) > 0;
            }
            else
            {
                goodTarget = true;
            }
        } while (!goodTarget);

        Instantiate(spawn, spawnPoint, Utils2D.LookRotation2D(targetPoint - spawnPoint));

        Invoke("Spawn", Random.Range(minDelay, maxDelay));
    }

    Vector3 PointInBox(Vector3 cornerA, Vector3 cornerB)
    {
        return new Vector3(cornerA.x < cornerB.x ? Random.Range(cornerA.x, cornerB.x) : Random.Range(cornerB.x, cornerA.x)
                        , cornerA.y < cornerB.y ? Random.Range(cornerA.y, cornerB.y) : Random.Range(cornerB.y, cornerA.y)
                        , cornerA.z < cornerB.z ? Random.Range(cornerA.z, cornerB.z) : Random.Range(cornerB.z, cornerA.z));
    }

    void OnDrawGizmos()
    {
        if (spawnCornerA != null && spawnCornerB != null)
        {
            Gizmos.color = Color.green;
            DrawWireCubeFromCorners(spawnCornerA.position, spawnCornerB.position);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (targetCornerA != null && targetCornerB != null)
        {
            Gizmos.color = Color.red;
            DrawWireCubeFromCorners(targetCornerA.position, targetCornerB.position);
        }
    }

    void DrawWireCubeFromCorners(Vector3 cornerA, Vector3 cornerB)
    {
        Vector3 dif = cornerB - cornerA;
        Vector3 center = cornerA + dif * .5f;

        Gizmos.DrawWireCube(center, dif);
    }
}
