﻿using UnityEngine;
using System.Collections;

public class InitialForce : MonoBehaviour
{

    public float force = 10;
    public ForceMode2D forceMode = ForceMode2D.Impulse;

    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, force), forceMode);
    }
}
