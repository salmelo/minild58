﻿using UnityEngine;
using System.Collections;

public class MoveToInterceptAlongLine : MonoBehaviour
{
    public LayerMask targetLayers = -1;
    public float acquireDistance = 8;
    public float speed = 2;

    [Tooltip("Will move in either direction of the defined line.")]
    public Vector2 direction = Vector2.right;

    [Tooltip("Will not move farther than this away from the origin.")]
    public Vector2 maxRange = new Vector2(8, 5);

    public int maxTargetCheckCount = 5;

    public bool ignoreBehind = false;

    new private Rigidbody2D rigidbody;
    private Rigidbody2D target;
    private Collider2D[] overlapResults;

    void Start()
    {
        overlapResults = new Collider2D[maxTargetCheckCount];
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        //Variable to store distance along targets velocity vector it will intersect with our line.
        float interceptDistance = -1;
        Vector2 interceptPoint;
        //Plane object used to find out if it will intercept and where.
        Plane interceptPlane = new Plane(transform.position
                                       , transform.position + transform.InverseTransformDirection(direction)
                                       , transform.position + Vector3.forward);

        switch (target != null)
        {
            //We already have a target, make sure it's still on an intercept course and update the distance.
            case true:
                //Check it's in front of us, if we care about that.
                if (ignoreBehind && !interceptPlane.GetSide(target.position))
                {
                    target = null;
                    goto case false;
                }

                Ray targetRay = new Ray(target.position, target.velocity);
                if (!interceptPlane.Raycast(targetRay, out interceptDistance))
                {
                    target = null;
                    goto case false;
                }

                interceptPoint = targetRay.GetPoint(interceptDistance);
                //Check if intercept point is within maxRange.
                if (interceptPoint.x > maxRange.x || interceptPoint.x < -maxRange.x
                    || interceptPoint.y > maxRange.y || interceptPoint.y < -maxRange.y)
                {
                    return;
                }
                break;

            //We don't have a target (or it moved off intercept course) find a new one.
            case false:
                //Get all potential targets in range.
                int possibleTargets = Physics2D.OverlapCircleNonAlloc(rigidbody.position, acquireDistance
                                                                    , overlapResults, targetLayers);

                // Find the closest one.
                Rigidbody2D closest = null;
                float closestDistance = float.PositiveInfinity;

                for (int i = 0; i < possibleTargets; i++)
                {
                    Rigidbody2D r = overlapResults[i].attachedRigidbody;
                    if (r == null) continue;

                    //Check it's in front of us, if we care about that.
                    if (ignoreBehind && !interceptPlane.GetSide(r.position))
                    {
                        continue;
                    }

                    float distance = (r.position - rigidbody.position).sqrMagnitude;
                    if (distance < closestDistance)
                    {
                        targetRay = new Ray(r.position, r.velocity);
                        // Only take new target if it's on an intercept course, and store intercept distance.
                        if (interceptPlane.Raycast(targetRay, out interceptDistance))
                        {
                            interceptPoint = targetRay.GetPoint(interceptDistance);
                            //Check if intercept point is within maxRange.
                            if (interceptPoint.x > maxRange.x || interceptPoint.x < -maxRange.x
                                || interceptPoint.y > maxRange.y || interceptPoint.y < -maxRange.y)
                            {
                                continue;
                            }

                            closest = r;
                            closestDistance = distance;
                        }
                    }
                }

                if (closest != null)
                {
                    target = closest;
                    targetRay = new Ray(target.position, target.velocity);
                    interceptPoint = targetRay.GetPoint(interceptDistance);
                }
                else
                {
                    return;
                }

                break;

            default : //This shouldn't happen.
                return;
        }

        //We have a target on an intercept course, head towards that point.
        rigidbody.MovePosition(Vector2.MoveTowards(rigidbody.position, interceptPoint, speed * Time.deltaTime));
    }
}
