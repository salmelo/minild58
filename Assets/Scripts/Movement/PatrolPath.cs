﻿using UnityEngine;
using System.Collections;

public class PatrolPath : MonoBehaviour
{

    public Transform[] positions;
    public float speed = 5;

    private int next;
    new private Rigidbody2D rigidbody;

    // Use this for initialization
    void Start()
    {
        next = 0;
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 target = positions[next].position;
        rigidbody.MovePosition(Vector2.MoveTowards(rigidbody.position, target, speed * Time.deltaTime));
        if (rigidbody.position == target)
        {
            next = (next + 1) % positions.Length;
        }
    }
}
