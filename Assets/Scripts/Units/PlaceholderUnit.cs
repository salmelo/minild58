﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlaceholderUnit : MonoBehaviour, IPointerClickHandler, IScrollHandler
{
    public float rotateStep = 180;
    public Color invalidTint = Color.red;

    public PlaceUnit Placer { get; set; }

    private PlacementChecker checker;
    new private SpriteRenderer renderer;
    private Color initialColor;

    void Start()
    {
        checker = GetComponent<PlacementChecker>();
        renderer = GetComponent<SpriteRenderer>();
        initialColor = renderer.color;
    }

    void Update()
    {
        if (checker.CanPlace())
        {
            renderer.color = initialColor;
        }
        else
        {
            renderer.color = initialColor * invalidTint;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        switch (eventData.button)
        {
            case PointerEventData.InputButton.Left:
                if (checker.CanPlace())
                {
                    Placer.Place(transform.position, transform.rotation);
                    Destroy(gameObject);

                    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    {
                        Placer.BeginPlace();
                    }
                }
                break;

            case PointerEventData.InputButton.Middle:
                Rotate(1);
                break;

            case PointerEventData.InputButton.Right:
                Placer.Cancel();
                Destroy(gameObject);
                break;
        }
    }

    public void OnScroll(PointerEventData eventData)
    {
        Rotate((int)eventData.scrollDelta.y);
    }

    void Rotate(int steps)
    {
        transform.Rotate(Vector3.forward, rotateStep * steps);
    }
}
