﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PositionCommandSender : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            SelectionManager.Instance.IssuePositionCommand(Camera.main.ScreenToWorldPoint(eventData.position));
        }
    }
}
