﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SelectRedirector : MonoBehaviour, IPointerClickHandler
{
    public SelectableUnit target;

    void Start()
    {
        if (target == null)
        {
            target = GetComponentInParent<SelectableUnit>();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SelectionManager.Instance.SelectUnit(target);
    }
}
