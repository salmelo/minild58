﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SelectionManager : Singleton<SelectionManager>
{
    public LayerMask selectionMask = -1;
    public LayerMask noSelectMask = -1;
    public int side = 0;

    private Physics2DRaycaster raycaster;
    private HashSet<SelectableUnit> selectedUnits = new HashSet<SelectableUnit>();

    private bool canSelect = true;
    public bool CanSelect
    {
        get { return canSelect; }
        set
        {
            canSelect = value;
            if (canSelect)
            {
                raycaster.eventMask = selectionMask;
            }
            else
            {
                raycaster.eventMask = noSelectMask;
            }
        }
    }

    void Start()
    {
        raycaster = FindObjectOfType<Physics2DRaycaster>();
    }

    public void SelectUnit(SelectableUnit unit, bool multiple = false)
    {
        if (unit.side != side)
        {
            return;
        }

        if (!multiple)
        {
            foreach (var u in selectedUnits)
            {
                u.Selected = false;
            }
            selectedUnits.Clear();
        }

        selectedUnits.Add(unit);
        unit.Selected = true;
    }

    public void IssuePositionCommand(Vector2 position)
    {
        foreach (var unit in selectedUnits)
        {
            ExecuteEvents.Execute<IPositionCommandReceiver>(
                    unit.gameObject, null, (x, y) => x.IssueCommand(position)
                );
        }
    }

    public void Deselect()
    {
        selectedUnits.Clear();
    }

    public void OnLevelWasLoaded(int level)
    {
        canSelect = true;
        raycaster = FindObjectOfType<Physics2DRaycaster>();
    }
}