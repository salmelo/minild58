﻿using UnityEngine;
using System.Collections;

public class UnitType : ScriptableObject
{
    public string unitName = "Unnamed Unit";
    public int cost = 1;
    public SelectableUnit unit;
    public PlaceholderUnit placeModel;
    public Sprite icon;

    public string desc = "Lorem ipsum doler set amet.";
}
