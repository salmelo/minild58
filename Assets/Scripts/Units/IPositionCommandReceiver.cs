﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public interface IPositionCommandReceiver : IEventSystemHandler 
{
    void IssueCommand(Vector2 position);
}
