﻿using UnityEngine;
using System.Collections;

public class MovableUnit : MonoBehaviour, IPositionCommandReceiver
{
    public Behaviour[] disabledWhileMoving;
    public Transform positionOffset;
    public float speed = 3;

    //private new Rigidbody2D rigidbody;

    void Start()
    {
        //rigidbody = GetComponent<Rigidbody2D>();
    }

    public void IssueCommand(Vector2 position)
    {
        StopAllCoroutines();
        StartCoroutine(MoveToTarget(position));
    }

    IEnumerator MoveToTarget(Vector2 target)
    {
        if (positionOffset != null)
        {
            target -= (Vector2)positionOffset.position - (Vector2)transform.position;
        }

        foreach (var b in disabledWhileMoving ){
            b.enabled = false;
        }

        yield return new WaitForFixedUpdate();

        while ((Vector2)transform.position != target)
        {
            transform.position = (Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime));
            yield return new WaitForFixedUpdate();
        }

        foreach (var b in disabledWhileMoving)
        {
            b.enabled = true;
        }
    }
}
