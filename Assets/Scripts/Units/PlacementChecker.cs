﻿using UnityEngine;
using System.Collections;

public abstract class PlacementChecker : MonoBehaviour 
{

    public abstract bool CanPlace();

}
