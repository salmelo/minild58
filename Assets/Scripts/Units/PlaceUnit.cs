﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class PlaceUnit : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UnitType unit;
    public int side = 0;

    public InteractableByPool interactable;

    public bool hideParentGroup = true;

    private CanvasGroup parentGroup;

    void Start()
    {
        if (interactable == null)
        {
            interactable = GetComponent<InteractableByPool>();
            setInteractable();
        }

        parentGroup = GetComponentInParent<CanvasGroup>();
    }

    public void setInteractable()
    {
        if (interactable == null) return;

        interactable.cost = unit.cost;
        interactable.side = side;
    }

    void HideGroup()
    {
        if (hideParentGroup && parentGroup != null)
        {
            parentGroup.alpha = 0;
            parentGroup.blocksRaycasts = false;
        }
    }

    void ShowGroup()
    {
        if (hideParentGroup && parentGroup != null)
        {
            parentGroup.alpha = 1;
            parentGroup.blocksRaycasts = true;
        }
    }

    public void BeginPlace()
    {
        if (PoolManager.Instance.GetAmount("Money", side) < unit.cost)
        {
            return;
        }

        var placeholder = (PlaceholderUnit)Instantiate(unit.placeModel);
        placeholder.Placer = this;

        SelectionManager.Instance.Deselect();
        SelectionManager.Instance.CanSelect = false;

        HideGroup();
    }

    public void Place(Vector3 position, Quaternion rotation)
    {
        SelectableUnit placed = (SelectableUnit)Instantiate(unit.unit, position, rotation);
        placed.side = side;

        SelectionManager.Instance.CanSelect = true;

        PoolManager.Instance.AddAmount("Money", side, -unit.cost);

        ShowGroup();
    }

    public void Cancel()
    {
        SelectionManager.Instance.CanSelect = true;

        ShowGroup();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (UnitToolTip.current != null)
        {
            UnitToolTip.current.SetToolTipText(unit);
            UnitToolTip.current.gameObject.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UnitToolTip.current.gameObject.SetActive(false);
    }
}
