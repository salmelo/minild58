﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitToolTip : MonoBehaviour
{

    public static UnitToolTip current;

    public Text text;

    // Use this for initialization
    void Start()
    {
        if (text == null)
        {
            text = GetComponentInChildren<Text>();
        }

        current = this;
        gameObject.SetActive(false);
    }

    public void SetToolTipText(UnitType unit)
    {
        text.text = string.Format("{0}\nCost: {1}\n{2}", unit.name, unit.cost.ToString(), unit.desc);
    }
}
