﻿using UnityEngine;
using System.Collections;

public class BoxPlacementChecker : PlacementChecker
{
    public LayerMask layerMask = -1;
    public Vector2 pointA = -Vector2.one;
    public Vector2 pointB = Vector2.one;

    private Collider2D[] hit = new Collider2D[1];

    public override bool CanPlace()
    {
        return Physics2D.OverlapAreaNonAlloc(transform.TransformPoint(pointA)
                                           , transform.TransformPoint(pointB)
                                           , hit, layerMask) < 1;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 a = transform.TransformPoint(pointA);
        Vector3 b = transform.TransformPoint(pointA.x, pointB.y, 0);
        Vector3 c = transform.TransformPoint(pointB);
        Vector3 d = transform.TransformPoint(pointB.x, pointA.y, 0);

        Gizmos.DrawLine(a, b);
        Gizmos.DrawLine(b, c);
        Gizmos.DrawLine(c, d);
        Gizmos.DrawLine(d, a);
    }
}
