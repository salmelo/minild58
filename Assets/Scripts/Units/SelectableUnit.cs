﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SelectableUnit : MonoBehaviour, IPointerClickHandler
{

    public Behaviour[] selectedBehaviours;
    public int side = 0;

    private bool selected = false;
    public bool Selected
    {
        get
        {
            return selected;
        }
        set
        {
            selected = value;
            foreach (var bev in selectedBehaviours)
            {
                bev.enabled = selected;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SelectionManager.Instance.SelectUnit(this);
    }
}
