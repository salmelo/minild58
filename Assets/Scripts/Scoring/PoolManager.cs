﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PoolManager : Singleton<PoolManager>
{
    //public int initialSideCount = 1;

    [SerializeField]
    private int sideCount = 1;

    private Dictionary<string, int[]> pools = new Dictionary<string, int[]>();

    public int SideCount { get { return sideCount; } }

    public string[] PoolList { get { return pools.Keys.ToArray(); } }

    [System.Serializable]
    public class PoolEvent : UnityEngine.Events.UnityEvent<string, int> { }
    public PoolEvent onPoolChanged;

    public HashSet<string> ResetOnLoad = new HashSet<string>(); //{ get; set; }

    public void SetSideCount(int sides)
    {
        if (sideCount == sides) return;

        sideCount = sides;

        foreach (string pool in pools.Keys.ToArray())
        {
            int[] array = pools[pool];
            System.Array.Resize(ref array, sides);
            pools[pool] = array;
        }
    }

    private void InitPool(string pool)
    {
        if (!pools.ContainsKey(pool))
        {
            pools.Add(pool, new int[sideCount]);
        }
    }

    private void InitPool(string pool, int[] values)
    {
        if (!pools.ContainsKey(pool) || ResetOnLoad.Contains(pool))
        {
            ResetOnLoad.Remove(pool);

            SetSideCount(values.Length);

            pools[pool] = values;
        }
    }

    private void RunInitializers()
    {
        foreach (var i in FindObjectsOfType<PoolInitializer>())
        {
            InitPool(i.pool, i.values);
            Destroy(i.gameObject);
        }
    }

    void Start()
    {
        RunInitializers();
    }

    public int GetAmount(string pool, int side)
    {
        InitPool(pool);

        return pools[pool][side];
    }

    public void SetAmount(string pool, int side, int value)
    {
        InitPool(pool);

        if (pools[pool][side] != value)
        {
            pools[pool][side] = value;

            onPoolChanged.Invoke(pool, side);
        }
    }

    public void AddAmount(string pool, int side, int value)
    {
        InitPool(pool);

        if (value != 0)
        {
            pools[pool][side] += value;

            onPoolChanged.Invoke(pool, side);
        }
    }

    protected override void ConsumeNewInstance(PoolManager newInstance)
    {
        onPoolChanged = newInstance.onPoolChanged;
    }

    public void OnLevelWasLoaded(int level)
    {
        RunInitializers();

        foreach (string pool in ResetOnLoad)
        {
            for (int i = 0; i < sideCount; i++)
            {
                pools[pool][i] = 0;
            }
        }

        ResetOnLoad.Clear();
    }


}