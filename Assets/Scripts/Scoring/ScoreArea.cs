﻿using UnityEngine;
using System.Collections;

public class ScoreArea : MonoBehaviour
{
    public string pool = "Score";
    public int side = 0;
    public int points = 1;

    public void OnTriggerEnter2D(Collider2D other)
    {
        PoolManager.Instance.AddAmount(pool, side, points);
    }

    public void OnDrawGizmos()
    {
        Collider2D col = GetComponent<Collider2D>();

        Gizmos.color = Color.yellow;
        if (col is BoxCollider2D)
        {
            Gizmos.DrawWireCube(col.bounds.center, col.bounds.size);
        }
    }

}
