﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeDisplay : MonoBehaviour
{

    public string format = "mm:ss";

    private Text text;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = System.DateTime.Today.AddSeconds(TimeManager.Instance.GameTime).ToString(format);
    }
}
