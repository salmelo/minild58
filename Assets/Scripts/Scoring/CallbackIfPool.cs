﻿using UnityEngine;
using System.Collections;

public class CallbackIfPool : MonoBehaviour
{

    public string pool = "Score";
    public int side = 0;
    public int value = 5;

    public CompareMode compareMode = CompareMode.Greater;

    public UnityEngine.Events.UnityEvent callback;

    public void CheckValue(string poolChanged, int sideChanged)
    {
        if (pool == poolChanged && side == sideChanged)
        {
            compareCallback(PoolManager.Instance.GetAmount(pool, side));
        }
    }

    public void CheckAnyValue(string poolChanged, int sideChanged)
    {
        if (pool == poolChanged)
        {
            compareCallback(PoolManager.Instance.GetAmount(pool, side));
        }
    }

    void compareCallback(int newValue)
    {
        if (compareMode.CompareInt(newValue,value))
        {
            callback.Invoke();
        }
    }
}
