﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractableByPool : MonoBehaviour
{

    public string pool = "Money";
    public int side = 0;
    public int cost = 1;

    public CompareMode compareMode = CompareMode.Greater;

    private Selectable selectable;

    void Start()
    {
        selectable = GetComponent<Selectable>();
        PoolManager.Instance.onPoolChanged.AddListener(poolChanged);
    }

    public void OnEnable()
    {
        if (selectable != null)
        {
            PoolManager.Instance.onPoolChanged.AddListener(poolChanged);
        }
    }

    public void OnDisable()
    {
        PoolManager.Instance.onPoolChanged.RemoveListener(poolChanged);
    }

    public void poolChanged(string pool, int side)
    {
        if (pool == this.pool && side == this.side)
        {
            if (compareMode.CompareInt(PoolManager.Instance.GetAmount(pool, side), cost))
            {
                selectable.interactable = true;
            }
            else
            {
                selectable.interactable = false;
            }
        }
    }
}
