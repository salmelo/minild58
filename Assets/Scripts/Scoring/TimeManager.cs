﻿using UnityEngine;
using System.Collections;

public class TimeManager : Singleton<TimeManager>
{
    public bool running = true;

    private float gameTime = 0;

    public float GameTime { get { return gameTime; } }

    public bool ResetOnLoad { get; set; }

    [System.Serializable]
    public class TimedCallback : System.IComparable<TimedCallback>
    {
        public float time = 0;
        public UnityEngine.Events.UnityEvent callback;

        public int CompareTo(TimedCallback other)
        {
            return Comparer.Default.Compare(this.time, other.time);
        }
    }

    public TimedCallback[] _callbacks;

    private Wintellect.PowerCollections.OrderedBag<TimedCallback> callbacks;

    void Start()
    {
        callbacks = new Wintellect.PowerCollections.OrderedBag<TimedCallback>(_callbacks);

        _callbacks = new TimedCallback[0];
    }

    public void Pause()
    {
        running = false;
    }

    public void Unpause()
    {
        running = true;
    }

    public TimedCallback AddCallback(float time, UnityEngine.Events.UnityEvent callback, bool timeFromNow = true)
    {
        if (timeFromNow)
        {
            time += gameTime;
        }

        TimedCallback timedCallback = new TimedCallback() { callback = callback, time = time };
        callbacks.Add(timedCallback);

        return timedCallback;
    }

    public void RemoveCallback(TimedCallback callback)
    {
        callbacks.RemoveAll(c => c == callback);
    }

    // Update is called once per frame
    void Update()
    {
        if (running)
        {
            gameTime += Time.deltaTime;

            while (callbacks.Count > 0 && callbacks.GetFirst().time <= gameTime)
            {
                callbacks.RemoveFirst().callback.Invoke();
            }
        }
    }

    public void OnLevelWasLoaded(int level)
    {
        if (ResetOnLoad)
        {
            gameTime = 0;
            ResetOnLoad = false;
        }
    }
}
