﻿using UnityEngine;
using System.Collections;

public enum CompareMode { Equal, NotEqual, Greater, Lesser }

public static class CompareModeExtensions
{
    public static bool CompareInt(this CompareMode mode, int a, int b)
    {
        switch (mode)
        {
            case CompareMode.Greater:
                return (a >= b);

            case CompareMode.Lesser:
                return (a <= b);

            case CompareMode.Equal:
                return (a == b);

            case CompareMode.NotEqual:
                return (a != b);

            default:
                Debug.LogError("CompareMode hit default, you probably added a new mode and forgot to put it in the switch.");
                return false;
        }
    }
}
